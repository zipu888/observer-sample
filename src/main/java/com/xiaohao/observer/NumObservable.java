package com.xiaohao.observer;

import java.util.Observable;

/**
 * Created by Administrator on 15-5-20.
 */
public class NumObservable extends Observable {

    private int data=0;

    public int getData() {
        return data;
    }

    public void setData(int i) {
        data = i;
        /* call super */
        setChanged();
        /* notify */
        notifyObservers();
    }
}
