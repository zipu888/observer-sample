package com.xiaohao.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * 观察者
 * Created by Administrator on 15-5-20.
 */
public class NumObserver implements Observer {


    @Override
    public void update(Observable o, Object arg) {
        NumObservable nobv =(NumObservable)o;
        System.out.println("data has change: "+nobv.getData());
    }
}
