package com.xiaohao.observer;

/**
 * Created by Administrator on 15-5-20.
 */
public class Startor {

    public static void main(String[] args){
        NumObservable numObservable = new NumObservable();
        numObservable.addObserver(new NumObserver());

        numObservable.setData(1);
        numObservable.setData(2);
        numObservable.setData(3);
        numObservable.setData(4);

    }

}
